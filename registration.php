<?php
/**
 * Copyright © Shine Software All rights reserved.
 * See COPYING.txt for license details.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Shinesoftware_Core', __DIR__);

