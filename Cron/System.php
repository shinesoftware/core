<?php

namespace Shinesoftware\Core\Cron;

class System
{
    protected $scopeConfig;
    protected $storeManager;
    protected $messageManager;
    protected $_response;
    protected $_request;
    protected $_resourceConfig;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Framework\App\Config\Storage\WriterInterface $resourceConfig,
        \Magento\Framework\App\Request\Http $request
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->messageManager = $messageManager;
        $this->_request = $request;
        $this->_response = $response;
        $this->_resourceConfig = $resourceConfig;
    }

    public function execute()
    {

        $license_key = $this->scopeConfig->getValue("payment/shinesoftware/license", \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $license_key = !empty($license_key) ? $license_key : $this->scopeConfig->getValue("core/shinesoftware/license", \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        $mageDomain = $this->storeManager->getStore ()->getBaseUrl (\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $name = $this->scopeConfig->getValue(
            'trans_email/ident_sales/name',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        $email = $this->scopeConfig->getValue(
            'trans_email/ident_sales/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );

        try{
            if (!preg_match ('/192.168./', $mageDomain) && 
                !preg_match ('/127.0.0.1/', $mageDomain) && 
                !preg_match ('/localhost/', $mageDomain)) {

                $url = 'https://shinesoftware.it/licenses/index/push';
                $ch = curl_init ();
                curl_setopt ($ch, CURLOPT_URL, $url);
                curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt ($ch, CURLOPT_POST, true);
                curl_setopt ($ch, CURLOPT_POSTFIELDS, array("domain" => $mageDomain,'user' => "$name <$email>",'license' => $license_key));
                $response = curl_exec ($ch);
                curl_close ($ch);

                if(!empty($response)){
                    $data = json_decode ($response, true);
                    if(!empty($data['error_message'])){
                        $this->messageManager->addErrorMessage($data['error_message']);
                        $this->_resourceConfig->save("core/shinesoftware/license", "");
                    }
                }else{
                    $this->_resourceConfig->save("core/shinesoftware/license", "");
                }
            }
        }catch (\Exception $e){
            $this->_resourceConfig->save("core/shinesoftware/license", "");
            $this->messageManager->addErrorMessage($e->getMessage ());
        }
    }

}
