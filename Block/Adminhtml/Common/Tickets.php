<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Core\Block\Adminhtml\Common;

class Tickets extends \Magento\Backend\Block\Template
{
    /**
     * Path to template file in theme
     *
     * @var string
     */
    protected $_template = 'tickets.phtml';
}
