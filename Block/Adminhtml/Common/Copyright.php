<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shinesoftware\Core\Block\Adminhtml\Common;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManager;

class Copyright extends \Magento\Backend\Block\Template
{
    /**
     * Path to template file in theme
     *
     * @var string
     */
    protected $_template = 'copyright.phtml';

    /**
     * storeManager
     *
     * @var StoreManager
     */
    protected $storeManager;
    
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;


    public function __construct(\Magento\Backend\Block\Template\Context $context, StoreManager $storeManager, ScopeConfigInterface $scopeConfig )
    {

        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;

        parent::__construct($context);
    }

    /**
     * get the worldwide preference from the database
     * @return boolean
     */
    public function showInDashboard() {
        return $this->scopeConfig->getValue('worldwide/options/dashboard', 'website', $this->storeManager->getWebsite()->getId());
    }
}
